#include "TbgeWindow.h"

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <string>
#include <vector>
#include <iostream>
#include <stdlib.h>

/// Constructor
/**
* @param heigth of the window. 800 by default.
* @param width of the window. 600 by default.
* @param title of the window. "Window" by default.
* 
* Starts with style "Close" and "Resize".
* Frame rate limit to 60.
*/
tbge::TbgeWindow::TbgeWindow(int heigth, int width, std::string title):
	heigth_(heigth),
	width_(width),
	title_(title),
	window_()
{
	window_ = new sf::RenderWindow();
	window_->create(sf::VideoMode(heigth_, width_), title_, sf::Style::Close | sf::Style::Resize);
	window_->setFramerateLimit(60);
}

/// GET heigth
/**
* @returns heigth of the window.
*/
int tbge::TbgeWindow::getHeigth()
{
	return heigth_;
}

/// GET width
/**
* @returns width of the window.
*/
int tbge::TbgeWindow::getWidth()
{
	return width_;
}

/// GET title
/**
* @returns title of the window.
*/
std::string tbge::TbgeWindow::getTitle()
{
	return title_;
}

/// SET heigth
/**
* @param heigth of the window.
*/
void tbge::TbgeWindow::setHeigth(int heigth)
{
	heigth_ = heigth;
}

/// SET width
/**
* @param width of the window.
*/
void tbge::TbgeWindow::setWidth(int width)
{
	width_ = width;
}

/// SET title
/**
* @param title of the window.
*/
void tbge::TbgeWindow::setTitle(std::string title)
{
	title_ = title;
	window_->setTitle(title);
}

/// SET position
/**
* @param x and y. Change the position of the window (relatively to the desktop).
*/
void tbge::TbgeWindow::setPosition(int x, int y)
{
	window_->setPosition(sf::Vector2i(x, y));
}

/// SET size
/**
* Change the size of the window.
*/
void tbge::TbgeWindow::setSize()
{
	window_->setSize(sf::Vector2u(width_, heigth_));
}

/// is open
/**
* @returns true if the window is open.
*/
bool tbge::TbgeWindow::isOpen() 
{
	return window_->isOpen();
}

/// poll event
/**
* @returns true if an event is triggered.
*/
bool tbge::TbgeWindow::pollEvent(sf::Event event) 
{
	return window_->pollEvent(event);
}

/// close
/**
* Close the window.
*/
void tbge::TbgeWindow::close() 
{
	window_->close();
}

/// 
/**
* clear the window.
*/
void tbge::TbgeWindow::clear()
{
	window_->clear();
}

/// 
/**
* @param sprite: draw the sprite.
*/
void tbge::TbgeWindow::draw(sf::Sprite sprite)
{
	window_->draw(sprite);
}

/// 
/**
* display the window.
*/
void tbge::TbgeWindow::display()
{
	window_->display();
}


/// Destructor
/**
* 
*/
tbge::TbgeWindow::~TbgeWindow()
{

}



