#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <string>
#include <vector>
#include <iostream>
#include <stdlib.h>


namespace tbge 
{
	class TbgeWindow
	{
	public:
		TbgeWindow(
			int heigth = 800, 
			int width = 600, 
			std::string title = "Window"
		);

		int getHeigth();
		int getWidth();
		std::string getTitle();

		void setHeigth(int heigth);
		void setWidth(int width);
		void setTitle(std::string title);

		void setPosition(int x, int y);
		void setSize();

		bool isOpen();
		bool pollEvent(sf::Event event);
		void close();
		void clear();
		void draw(sf::Sprite sprite);
		void display();

		~TbgeWindow();

	private:
		int heigth_; 
		int width_; 
		std::string title_;
		sf::RenderWindow* window_;
	};
}