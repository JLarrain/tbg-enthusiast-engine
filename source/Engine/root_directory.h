/*
	This field was automatically created with CMake please don't modify it
*/
#pragma once

#include <filesystem>

namespace Engine
{
	
static const char * const source_directory = "D:/Academico/Ramos_actuales/Game_engine/tbg-enthusiast-engine/";

static std::filesystem::path getPath(const std::string &relative_path){
	return source_directory + relative_path;
}
	
} // Engine
