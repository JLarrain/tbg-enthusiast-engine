#include "Engine/TbgeWindow.h"

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

int main()
{
    tbge::TbgeWindow *testWindow = new tbge::TbgeWindow(400, 400, "stay enthusiastic!");

    // run the program as long as the window is open
    while (testWindow->isOpen())
    {
        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
        while (testWindow->pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
                testWindow->close();
        }
    }

    return 0;

}
