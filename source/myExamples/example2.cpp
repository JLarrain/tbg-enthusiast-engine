#include "Engine/TbgeWindow.h"
#include "Engine/root_directory.h"

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>


int main()
{
    tbge::TbgeWindow *testWindow = new tbge::TbgeWindow(400, 400, "stay enthusiastic!");


    sf::Texture texture;
    if (!texture.loadFromFile(Engine::getPath("assets/imgs/Icono_256x256_sinFondo.png").string())) 
    {
        // TO DO: Manejo de errores
        std::cerr << "No se llama..." << std::endl;
    }
    //texture.setSmooth(false);
    //texture.setRepeated(false);

    sf::Sprite sprite;
    sprite.setTexture(texture);

    

    // run the program as long as the window is open
    while (testWindow->isOpen())
    {

        testWindow->clear();
        testWindow->draw(sprite);
        testWindow->display();


        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
        while (testWindow->pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
                testWindow->close();

        }        
    }


    return 0;
	
}
